package com.scalangular.core.services

import com.scalangular.core.services.jwt.DefaultJWTService
import com.scalangular.core.services.jwt.JWTService
import com.scalangular.core.services.jwt.JWTServiceConfig
import com.scalangular.core.services.user.BCryptUserService
import com.scalangular.core.services.user.UserService
import com.scalangular.core.services.database.DatabaseService
import com.scalangular.core.services.database.DatabaseServiceConfig
import com.scalangular.core.services.database.exposed.ExposedHakiriDatabaseService

class DefaultServiceFactory : ServiceFactory {
    override fun buildJWTService(config: JWTServiceConfig): JWTService {
        return DefaultJWTService(
            config.algorithm,
            config.refreshTokenExpiration,
            config.accessTokenExpiration,
            config.issuer
        )
    }

    override fun buildDatabaseService(config: DatabaseServiceConfig): DatabaseService {
        return ExposedHakiriDatabaseService(config)
    }

    override fun buildUserService(): UserService {
        return BCryptUserService()
    }
}