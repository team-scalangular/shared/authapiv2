package com.scalangular.core.services

import com.scalangular.core.services.jwt.JWTService
import com.scalangular.core.services.jwt.JWTServiceConfig
import com.scalangular.core.services.user.UserService
import com.scalangular.core.services.database.DatabaseService
import com.scalangular.core.services.database.DatabaseServiceConfig

interface ServiceFactory {
    fun buildJWTService(config: JWTServiceConfig): JWTService
    fun buildDatabaseService(config: DatabaseServiceConfig): DatabaseService
    fun buildUserService(): UserService
}