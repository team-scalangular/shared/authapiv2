package com.scalangular.core.services.jwt

import arrow.core.Either
import com.auth0.jwt.algorithms.Algorithm
import com.scalangular.core.entities.Claim
import com.scalangular.core.entities.ScalAngularUser
import java.time.Duration

interface JWTServiceConfig {
    val algorithm: Algorithm
    val refreshTokenExpiration: Duration
    val accessTokenExpiration: Duration
    val issuer: String
}

interface JWTService {
    fun createRefreshToken(scalAngularUser: ScalAngularUser): String
    fun createAccessToken(scalAngularUser: ScalAngularUser, claims: List<Claim>): String
    fun validateAccessToken(token: String, resource: String, level: Int): Either<JWTError, Unit>
    fun validateRefreshToken(refreshToken: String): Either<JWTError, Unit>
    fun getUsernameFromToken(token: String): Either<JWTError, String>

    sealed class JWTError {
        data class TokenIsExpired(val message: String) : JWTError()

        data class InsufficientClaims(
            val resource: String,
            val requestedOperationLevel: Int,
            val providedLevel: Int
        ) : JWTError()

        data class MissingClaim(val resource: String) : JWTError()

        data class TokenIsInvalid(val message: String) : JWTError()
    }
}