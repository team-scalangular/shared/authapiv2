package com.scalangular.core.services.jwt

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.InvalidClaimException
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.exceptions.SignatureVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import com.auth0.jwt.interfaces.DecodedJWT
import com.scalangular.core.entities.Claim
import com.scalangular.core.entities.ScalAngularUser
import com.scalangular.core.services.jwt.JWTService.JWTError
import com.scalangular.core.services.jwt.JWTService.JWTError.*
import com.scalangular.utils.LoggerDelegate
import java.time.Duration
import java.time.Instant
import java.util.*


class DefaultJWTService(
    private val algorithm: Algorithm,
    private val refreshTokenExpiration: Duration,
    private val accessTokenExpiration: Duration,
    private val issuer: String
) : JWTService {

    private val logger by LoggerDelegate()

    private val refreshVerifier = JWT.require(algorithm)
        .withIssuer(issuer)
        .withSubject("refresh token")
        .build()

    private val accessVerifier = JWT.require(algorithm)
        .withIssuer(issuer)
        .withSubject("access token")
        .build()

    private fun withClaimLevel(decodedJWT: DecodedJWT, claim: String, minLevel: Int): Either<JWTError, Unit> {
        val decodedClaim = decodedJWT.getClaim(claim)

        return Either.conditionally(!decodedClaim.isNull,
            {
                logger.info("The token misses claim $claim")
                MissingClaim(claim) },
            { decodedClaim.asInt() })
            .flatMap { level -> checkLevelForSufficient(level, minLevel, claim) }
    }

    private fun checkLevelForSufficient(
        level: Int,
        minLevel: Int,
        claim: String
    ) = if (level >= minLevel) Unit.right()
    else InsufficientClaims(claim, minLevel, level).left()

    override fun createRefreshToken(scalAngularUser: ScalAngularUser): String {
        logger.info("Creating refresh token for ${scalAngularUser.username}")
        return JWT.create()
            .withClaim("username", scalAngularUser.username)
            .withIssuer(issuer)
            .withSubject("refresh token")
            .withExpiresAt(Date.from(Instant.now().plus(refreshTokenExpiration)))
            .sign(algorithm)
    }

    override fun createAccessToken(scalAngularUser: ScalAngularUser, claims: List<Claim>): String {
        logger.info("Creating access token for ${scalAngularUser.username}")
        val jwtBuilder = JWT.create()
            .withClaim("username", scalAngularUser.username)
            .withIssuer(issuer)
            .withSubject("access token")
            .withExpiresAt(Date.from(Instant.now().plus(accessTokenExpiration)))

        return claims.fold(jwtBuilder) { builder, claim ->
            builder.withClaim(claim.resource, claim.level)
        }.sign(algorithm)
    }

    override fun validateAccessToken(token: String, resource: String, level: Int): Either<JWTError, Unit> {
        logger.info("verifying access token $resource with min level $level")
        return verifyToken(token, accessVerifier).flatMap { decodedJWT ->
            withClaimLevel(decodedJWT, resource, level)
        }
    }

    override fun validateRefreshToken(refreshToken: String): Either<JWTError, Unit> {
        return verifyToken(refreshToken, refreshVerifier).map { }
    }

    private fun verifyToken(token: String, jwtVerifier: JWTVerifier): Either<JWTError, DecodedJWT> {
        return Either.catch {
            jwtVerifier.verify(token)
        }.mapLeft { error ->
            logger.info("Token could not be verified. Reason ${error.message}")
            handleJwtExceptions(error)
        }
    }

    private fun handleJwtExceptions(it: Throwable) = when (it) {
        is InvalidClaimException -> TokenIsInvalid("Claim is missing")
        is TokenExpiredException -> TokenIsExpired(it.message!!)
        is SignatureVerificationException -> TokenIsInvalid("Could not verify signature")
        is JWTVerificationException -> TokenIsInvalid("One ore more verification steps failed")
        else -> TokenIsInvalid("Unknown error occurred while validating")
    }

    override fun getUsernameFromToken(token: String): Either<JWTError, String> {
        return Either.catch {
            refreshVerifier.verify(token)
        }.map { decodedJWT ->
            decodedJWT.getClaim("username").asString()
        }.mapLeft { error ->
            handleJwtExceptions(error)
        }
    }
}