package com.scalangular.core.services.user

import com.scalangular.core.entities.ScalAngularUser

interface UserService {
    fun checkPassword(password: String, scalAngularUser: ScalAngularUser): Boolean
    fun hashPassword(password: String): String
}