package com.scalangular.core.services.user

import at.favre.lib.crypto.bcrypt.BCrypt
import com.scalangular.core.entities.ScalAngularUser
import com.scalangular.utils.LoggerDelegate

class BCryptUserService : UserService {

    private val passwordHasher = BCrypt.withDefaults()
    private val passwordVerifier = BCrypt.verifyer()

    private val logger by LoggerDelegate()

    override fun checkPassword(password: String, scalAngularUser: ScalAngularUser): Boolean {
        logger.info("Checking password for user ${scalAngularUser.username}")

        val hash = scalAngularUser.passwordHash
        val result = passwordVerifier.verify(password.toCharArray(), hash.toCharArray())

        if (result.verified) logger.info("Password for user ${scalAngularUser.username} was valid")
        else logger.info("Password for user ${scalAngularUser.username} was invalid. Reason: ${result.formatErrorMessage}")

        return result.verified
    }

    override fun hashPassword(password: String): String {
        val hashedPassword = passwordHasher.hashToChar(15, password.toCharArray())

        return String(hashedPassword);
    }

}