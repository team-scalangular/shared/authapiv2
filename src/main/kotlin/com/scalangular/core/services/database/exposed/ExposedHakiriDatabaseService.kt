package com.scalangular.core.services.database.exposed

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.traverseEither
import com.scalangular.core.entities.Claim
import com.scalangular.core.entities.ScalAngularUser
import com.scalangular.core.services.database.DatabaseService
import com.scalangular.core.services.database.DatabaseService.DatabaseServiceError
import com.scalangular.core.services.database.DatabaseService.DatabaseServiceError.*
import com.scalangular.core.services.database.DatabaseServiceConfig
import com.scalangular.utils.LoggerDelegate
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*


class ExposedHakiriDatabaseService(
    dataSourceConfig: DatabaseServiceConfig
) : DatabaseService {

    private val logger by LoggerDelegate()

    private fun initDataSource(dataSourceConfig: DatabaseServiceConfig): Database {
        logger.info("Initializing database connection with url ${dataSourceConfig.jdbcUrl}")

        val hikariConfig = HikariConfig()
        hikariConfig.jdbcUrl = dataSourceConfig.jdbcUrl
        hikariConfig.username = dataSourceConfig.username
        hikariConfig.password = dataSourceConfig.password
        hikariConfig.maximumPoolSize = 2

        // TODO handle connection error
        return Database.connect(HikariDataSource(hikariConfig))
    }

    private fun createTablesIfNotExistent(database: Database) {
        logger.info("creating tables if not existing")
        transaction(database) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(ScalAngularUsers, Claims)
        }
    }

    private val database = initDataSource(dataSourceConfig)

    init {
        createTablesIfNotExistent(database)
    }

    private fun <T> ResultRow.getOrLeft(c: Expression<T>): Either<DatabaseServiceError, T> {
        return Either.fromNullable(
            getOrNull(c)
        ).mapLeft { CouldNotBuildUser }
    }

    private fun ResultRow.asScalangularUser(): Either<DatabaseServiceError, ScalAngularUser> {
        return this.getOrLeft(ScalAngularUsers.userId).flatMap { userId ->
            this.getOrLeft(ScalAngularUsers.userName).flatMap { username ->
                this.getOrLeft(ScalAngularUsers.passwordHash).map { passwordHash ->
                    ScalAngularUser(userId, username, passwordHash)
                }
            }
        }
    }

    private fun ResultRow.asClaim(): Either<DatabaseServiceError, Claim> {
        return this.getOrLeft(Claims.claimId).flatMap { id ->
            this.getOrLeft(Claims.resource).flatMap { resource ->
                this.getOrLeft(Claims.level).map { level ->
                    Claim(
                        id,
                        resource,
                        level
                    )
                }
            }
        }
    }

    private fun <T, L> saveTransaction(
        db: Database? = null,
        statement: Transaction.() -> T,
        errorHandling: (Throwable) -> L
    ): Either<L, T> {
        return Either.catch(
            { error ->
                logger.info("Couldn't execute")
                logger.info("${error.cause}: ${error.message}")
                errorHandling(error)
            },
            { transaction(db) { statement() } }
        )
    }

    override fun getUserByUsername(username: String): Either<DatabaseServiceError, ScalAngularUser> {
        return saveTransaction(database,
            {
                ScalAngularUsers.select {
                    ScalAngularUsers.userName.eq(username)
                }.single()
            },
            { NoUserByUsername(username) })
            .flatMap { resultRow ->
                resultRow.asScalangularUser()
            }
    }

    override fun deleteUserById(id: UUID): Either<DatabaseServiceError, Int> {
        return saveTransaction(database, {
            ScalAngularUsers.deleteWhere {
                ScalAngularUsers.userId.eq(id)
            }
        }, {
            NoUserById(id)
        })
    }

    override fun addUser(
        userNameToInsert: String,
        passwordHashToInsert: String
    ): Either<DatabaseServiceError, UUID> {
        return saveTransaction(database,
            {
                ScalAngularUsers.insert {
                    it[userName] = userNameToInsert
                    it[passwordHash] = passwordHashToInsert
                }[ScalAngularUsers.userId]
            },
            {
                CouldNotCreateUser(userNameToInsert)
            }
        )
    }

    override fun updateUser(scalAngularUserWithUpdates: ScalAngularUser): Either<DatabaseServiceError, ScalAngularUser> {
        return saveTransaction(database,
            {
                ScalAngularUsers.update({
                    ScalAngularUsers.userId.eq(scalAngularUserWithUpdates.userId)
                }) {
                    it[userName] = scalAngularUserWithUpdates.username
                    it[passwordHash] = scalAngularUserWithUpdates.passwordHash
                }
            },
            {
                CouldNotUpdateUser(scalAngularUserWithUpdates.userId)
            }
        ).flatMap {
            getUserByUsername(scalAngularUserWithUpdates.username)
        }
    }


    // TODO: Check if traverse flow is possible
    override fun getClaims(userId: UUID): Either<DatabaseServiceError, List<Claim>> {
        return saveTransaction(database,
            {
                Claims.select { Claims.userId eq userId }.map { it.asClaim() }
            },
            {
                CouldNotGetClaims(userId)
            })
            .flatMap {
                it.traverseEither { it }
            }
    }


    override fun addClaims(
        userIdToAddClaimsToo: UUID,
        claims: List<Claim>
    ): Either<DatabaseServiceError, List<Claim>> {
        return saveTransaction(database,
            {
                claims.map { claimToInsert ->
                    Claims.insert {
                        it[resource] = claimToInsert.resource
                        it[level] = claimToInsert.level
                        it[userId] = userIdToAddClaimsToo
                    }[Claims.claimId]
                }
            },
            {
                CouldNotAddClaims(userIdToAddClaimsToo, claims)
            }).flatMap { getClaims(userIdToAddClaimsToo) }
    }
}