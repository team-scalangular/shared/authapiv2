package com.scalangular.core.services.database

import arrow.core.Either
import com.scalangular.core.entities.Claim
import com.scalangular.core.entities.ScalAngularUser
import java.util.*

interface DatabaseServiceConfig {
    val password: String
    val username: String
    val jdbcUrl: String
}

interface DatabaseService {
    fun getUserByUsername(username: String): Either<DatabaseServiceError, ScalAngularUser>

    fun deleteUserById(id: UUID): Either<DatabaseServiceError, Int>

    fun addUser(userNameToInsert: String, passwordHashToInsert: String): Either<DatabaseServiceError, UUID>

    fun updateUser(scalAngularUserWithUpdates: ScalAngularUser): Either<DatabaseServiceError, ScalAngularUser>

    fun getClaims(userId: UUID): Either<DatabaseServiceError, List<Claim>>

    fun addClaims(userIdToAddClaimsToo: UUID, claims: List<Claim>): Either<DatabaseServiceError, List<Claim>>

    sealed class DatabaseServiceError {
        data class NoUserByUsername(val username: String): DatabaseServiceError()
        data class NoUserById(val id: UUID): DatabaseServiceError()
        object CouldNotBuildUser: DatabaseServiceError()
        data class CouldNotCreateUser(val username: String): DatabaseServiceError()
        data class CouldNotUpdateUser(val userId: UUID): DatabaseServiceError()
        data class CouldNotAddClaims(val userId: UUID, val claims: List<Claim>): DatabaseServiceError()
        data class CouldNotGetClaims(val userId: UUID): DatabaseServiceError()
    }
}