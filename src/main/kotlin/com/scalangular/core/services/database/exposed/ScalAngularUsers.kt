package com.scalangular.core.services.database.exposed

import org.jetbrains.exposed.sql.Table

object ScalAngularUsers : Table() {
    val userId = uuid("userId").autoGenerate().primaryKey()
    val userName = varchar("userName", 25).uniqueIndex()
    val passwordHash = varchar("passwordHash", 128)
}