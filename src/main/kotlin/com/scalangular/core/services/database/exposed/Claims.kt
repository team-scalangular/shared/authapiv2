package com.scalangular.core.services.database.exposed

import org.jetbrains.exposed.sql.Table

object Claims: Table() {
    val claimId = uuid("claimId").autoGenerate().primaryKey()
    val userId = reference("userId", ScalAngularUsers.userId)
    val resource = varchar("resource", 50)
    val level = integer("level")
}
