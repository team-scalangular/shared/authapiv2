package com.scalangular.core.entities

import java.util.*

data class Claim(val userId: UUID?, val resource: String, val level: Int)