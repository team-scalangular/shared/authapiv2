package com.scalangular.core.entities

import java.util.*

data class ScalAngularUser(val userId: UUID, val username: String, val passwordHash: String)