package com.scalangular.api

import arrow.core.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*

data class ArrowResponse<T>(val status: HttpStatusCode, val message: Option<T>)

fun <T> ok(): ArrowResponse<T> = status(HttpStatusCode.OK)

fun <T> ok(content: T): ArrowResponse<T> = status(HttpStatusCode.OK, content)

fun <T> status(status: HttpStatusCode, content: T) = ArrowResponse(status, content.some())

fun <T> status(status: HttpStatusCode) = ArrowResponse<T>(status, none())

@JvmName("arrowResponse")
suspend inline fun <reified R : Any> ApplicationCall.arrowResponse(response: Either<ArrowResponse<*>, R>){
    when(response){
        is Either.Left ->
        {
            arrowResponse(response.value)
        }

        is Either.Right -> arrowResponse(ok(response.value))
    }
}

@JvmName("arrowResponseMergeable")
suspend inline fun ApplicationCall.arrowResponse(response: Either<ArrowResponse<*>, ArrowResponse<*>>){
    when(response){
        is Either.Left -> arrowResponse(response.value)
        is Either.Right -> arrowResponse(response.value)
    }
}

suspend inline fun ApplicationCall.arrowResponse(arrowResponse: ArrowResponse<*>){
    when(arrowResponse.message){
        None -> respond(arrowResponse.status)
        is Some -> respond(arrowResponse.status, arrowResponse.message.value as Any)
    }
}