package com.scalangular

import arrow.core.*
import arrow.core.computations.either
import com.auth0.jwt.algorithms.Algorithm
import com.scalangular.api.ArrowResponse
import com.scalangular.api.arrowResponse
import com.scalangular.api.models.ScalAngularUser
import com.scalangular.api.ok
import com.scalangular.api.status
import com.scalangular.core.services.DefaultServiceFactory
import com.scalangular.core.services.database.DatabaseService.*
import com.scalangular.core.services.database.DatabaseServiceConfig
import com.scalangular.core.services.jwt.JWTService.*
import com.scalangular.core.services.jwt.JWTServiceConfig
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.pipeline.*
import org.slf4j.LoggerFactory
import java.time.Duration

fun <T> getFromEnvironment(property: String, f: (String) -> T): T = f(getFromEnvironment(property))
fun getFromEnvironment(property: String): String =
    System.getenv(property) ?: throw NullPointerException("$property was not found in environment but is required")


fun main() {
    val logger = LoggerFactory.getLogger("AuthApiV2")
    logger.info("Starting AuthApiV2")
    val serviceFactory = DefaultServiceFactory()

    logger.info("Creating userService")
    val userService = serviceFactory.buildUserService()
    logger.info("created")

    logger.info("Creating jwt service")
    val jwtSecrete = getFromEnvironment("jwtSecrete")
    val refreshDuration = getFromEnvironment("refreshDuration") { it.toLong() }
    val accessDuration = getFromEnvironment("accessDuration") { it.toLong() }
    val issuer = getFromEnvironment("issuer")
    val jwtService = serviceFactory.buildJWTService(
        object : JWTServiceConfig {
            override val algorithm: Algorithm
                get() = Algorithm.HMAC512(jwtSecrete)
            override val refreshTokenExpiration: Duration
                get() = Duration.ofSeconds(refreshDuration)
            override val accessTokenExpiration: Duration
                get() = Duration.ofSeconds(accessDuration)
            override val issuer: String
                get() = issuer
        }
    )
    logger.info("created")

    logger.info("Creating database service")
    val dbService = serviceFactory.buildDatabaseService(
        object : DatabaseServiceConfig {
            override val password: String
                get() = getFromEnvironment("dbPassword")
            override val username: String
                get() = getFromEnvironment("username")
            override val jdbcUrl: String
                get() = getFromEnvironment("jdbcUrl")
        }
    )
    logger.info("created")

    fun <T> Either<JWTError, T>.mapLeftToHttp(): Either<ArrowResponse<String>, T> {
        return this.mapLeft {
            when (it) {
                is JWTError.InsufficientClaims -> status(
                    HttpStatusCode.Unauthorized,
                    "Insufficient claim level. Provided ${it.providedLevel} but needs ${it.requestedOperationLevel}"
                )
                is JWTError.MissingClaim -> status(HttpStatusCode.Unauthorized, "Claim not available")
                is JWTError.TokenIsExpired -> status(HttpStatusCode.Unauthorized, "Token is expired")
                is JWTError.TokenIsInvalid -> status(HttpStatusCode.Unauthorized, "Token is invalid")
            }
        }
    }

    fun <T> Either<DatabaseServiceError, T>.mapLeftToHttp(): Either<ArrowResponse<*>, T> {
        return this.mapLeft {
            when (it) {
                is DatabaseServiceError.CouldNotAddClaims -> status(
                    HttpStatusCode.BadRequest,
                    "Cannot add claim ${it.claims} to user"
                )
                DatabaseServiceError.CouldNotBuildUser -> status(
                    HttpStatusCode.InternalServerError,
                    "Could not build user"
                )
                is DatabaseServiceError.CouldNotCreateUser -> status(
                    HttpStatusCode.BadRequest,
                    "Could not create user with username ${it.username}"
                )
                is DatabaseServiceError.CouldNotGetClaims -> status(
                    HttpStatusCode.InternalServerError,
                    "Could not get user claims"
                )
                is DatabaseServiceError.CouldNotUpdateUser -> status(
                    HttpStatusCode.BadRequest,
                    "Could not perform the update"
                )
                is DatabaseServiceError.NoUserById -> status(HttpStatusCode.NotFound, "User was not found")
                is DatabaseServiceError.NoUserByUsername -> status(HttpStatusCode.NotFound)
            }
        }
    }

    logger.info("Creating server")
    embeddedServer(Netty, port = 8080) {
        routing {
            post("/user") {
                val result = getTokenFromRequest()
                    .flatMap { token -> jwtService.validateAccessToken(token, "SCUSER", 4).mapLeftToHttp() }
                    .flatMap {
                        val userToCreate = call.receive<ScalAngularUser>()
                        val password = userService.hashPassword(userToCreate.password)
                        dbService.addUser(userToCreate.username, password).mapLeftToHttp()
                    }.map {
                        ok(it.toString())
                    }

                call.arrowResponse(result)
            }

            delete("/user") {
                val result = getTokenFromRequest()
                    .flatMap { token -> jwtService.validateAccessToken(token, "SCUSER", 3).mapLeftToHttp() }
                    .flatMap {
                        Option.fromNullable(call.request.queryParameters["username"]).toEither {
                            status(HttpStatusCode.BadRequest, "No username provided")
                        }
                    }.flatMap { username ->
                        dbService.getUserByUsername(username).mapLeftToHttp()
                    }.flatMap {
                        dbService.deleteUserById(it.userId).mapLeftToHttp()
                    }

                call.arrowResponse(result)
            }

            post("/access") {
                val results: Either<ArrowResponse<*>, String> = either.eager {
                    val token: String = Either.fromNullable(call.request.header(HttpHeaders.Authorization)).map {
                        it.substring(7)
                    }.mapLeft {
                        status(HttpStatusCode.BadRequest, "Token is missing")
                    }.bind()
                    jwtService.validateRefreshToken(token).mapLeftToHttp().bind()
                    val username = jwtService.getUsernameFromToken(token).mapLeftToHttp().bind()
                    val user = dbService.getUserByUsername(username).mapLeftToHttp().bind()
                    val claims = dbService.getClaims(user.userId).mapLeftToHttp().bind()
                    jwtService.createAccessToken(user, claims)
                }

                call.arrowResponse(results)
            }

            post("/auth") {
                val user = call.receive<ScalAngularUser>()

                val result = dbService.getUserByUsername(user.username)
                    .mapLeftToHttp()
                    .flatMap { dbUser ->
                        if (userService.checkPassword(user.password, dbUser)) {
                            jwtService.createRefreshToken(dbUser).right()
                        } else {
                            status(HttpStatusCode.Unauthorized, "Password or username invalid").left()
                        }
                    }

                call.arrowResponse(result)
            }

            get("/validate") {
                val result = either.invoke<ArrowResponse<*>, Unit> {
                    val resource: String = Option.fromNullable(call.request.queryParameters["resource"])
                        .toEither { status<Any>(HttpStatusCode.BadRequest) }.bind()
                    val level: Int = Option.fromNullable(call.request.queryParameters["level"]).map { it.toInt() }
                        .toEither { status<Any>(HttpStatusCode.BadRequest) }.bind()
                    val token: String = getTokenFromRequest().bind()

                    jwtService.validateAccessToken(token, resource, level).mapLeftToHttp().bind()
                }.swap().getOrElse { ok<Any>() }

                call.arrowResponse(result)
            }
        }

        install(ContentNegotiation) {
            gson {
                setPrettyPrinting()
                disableHtmlEscaping()
            }
        }

        install(CORS) {
            method(HttpMethod.Delete)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            anyHost()
            header(HttpHeaders.Authorization)
            allowCredentials = true
            allowNonSimpleContentTypes = true
        }

        install(StatusPages) {
            exception<IllegalArgumentException> { cause ->
                logger.error(cause.message)
                call.respond(HttpStatusCode.BadRequest)
            }
        }

        logger.info("created")

    }.start(wait = true)

}

private fun PipelineContext<Unit, ApplicationCall>.getTokenFromRequest() =
    Option.fromNullable(call.request.header(HttpHeaders.Authorization))
        .map { it.substring(7) }.toEither { status<Any>(HttpStatusCode.BadRequest) }


