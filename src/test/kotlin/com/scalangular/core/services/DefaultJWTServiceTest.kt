package com.scalangular.core.services

import arrow.core.getOrElse
import com.auth0.jwt.algorithms.Algorithm
import com.scalangular.core.entities.Claim
import com.scalangular.core.entities.ScalAngularUser
import com.scalangular.core.services.jwt.DefaultJWTService
import com.scalangular.core.services.jwt.JWTService.JWTError.*
import io.kotest.assertions.arrow.either.shouldBeLeft
import io.kotest.assertions.arrow.either.shouldBeRight
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.string.shouldHaveMinLength
import io.kotest.matchers.types.shouldBeInstanceOf
import java.time.Duration
import java.util.*

internal class DefaultJWTServiceTest : StringSpec({

    "refresh token can be generated" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ), Duration.ofDays(1), Duration.ofSeconds(10),"ScalangularCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val token = jwtService.createRefreshToken(user)

        assertSoftly(token) {
            isNotBlank()
            isNotEmpty()
            shouldHaveMinLength(12)
        }
    }

    "username is in refresh token claim" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ), Duration.ofDays(1), Duration.ofSeconds(10),"ScalangularUserClaimTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val token = jwtService.createRefreshToken(user)
        val extractedUserName = jwtService.getUsernameFromToken(token)

        extractedUserName shouldBeRight user.username
    }

    "refresh is expired" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ), Duration.ofSeconds(-5), Duration.ofSeconds(10),"ScalangularExperationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val token = jwtService.createRefreshToken(user)
        val verified = jwtService.validateRefreshToken(token)

        assertSoftly(verified) {
            shouldBeLeft()
            it.swap().getOrElse { null }.shouldBeInstanceOf<TokenIsExpired>()
        }
    }

    "refresh token is malformed" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ), Duration.ofDays(1), Duration.ofSeconds(10),"ScalangularMailfirnedTest"
        )

        val token = "abc"
        val verified = jwtService.validateRefreshToken(token)

        assertSoftly(verified) {
            shouldBeLeft()
            it.swap().getOrElse { null }.shouldBeInstanceOf<TokenIsInvalid>()
        }
    }

    "refresh token can be validated" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ), Duration.ofSeconds(10), Duration.ofSeconds(10),"ScalangularExperationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val token = jwtService.createRefreshToken(user)
        val verified = jwtService.validateRefreshToken(token)

        verified.shouldBeRight()
    }

    "access token can be generated" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ),
            Duration.ofDays(1),
            Duration.ofSeconds(10),
            "ScalangularAccessTokenCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val userId = user.userId
        val claims = listOf(
            Claim(userId, "users", 3),
            Claim(userId, "league", 2),
            Claim(userId, "race", 1)
        )

        val token = jwtService.createAccessToken(user, claims)

        assertSoftly(token) {
            isNotBlank()
            isNotEmpty()
            shouldHaveMinLength(12)
        }
    }

    "access token contains all claims" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ),
            Duration.ofDays(1),
            Duration.ofSeconds(10),
            "ScalangularAccessTokenCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val userId = user.userId
        val claims = listOf(
            Claim(userId, "users", 3),
            Claim(userId, "league", 2),
            Claim(userId, "race", 1)
        )

        val token = jwtService.createAccessToken(user, claims)

        assertSoftly(token){
            claims.forEach { claim ->
                jwtService.validateAccessToken(token, claim.resource, claim.level).shouldBeRight()
            }
        }
    }

    "access token is invalid if required claim is not found" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ),
            Duration.ofDays(1),
            Duration.ofDays(10),
            "ScalangularAccessTokenCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val userId = user.userId
        val claims = listOf(
            Claim(userId, "users", 3),
            Claim(userId, "league", 2),
            Claim(userId, "race", 1)
        )

        val token = jwtService.createAccessToken(user, claims)
        val verified = jwtService.validateAccessToken(token, "points", 2)

        assertSoftly(verified) {
            isLeft()
            it.swap().getOrElse { null }.shouldBeInstanceOf<MissingClaim>()
        }
    }

    "access token is invalid if required level is higher then the found one" {
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ),
            Duration.ofDays(1),
            Duration.ofDays(10),
            "ScalangularAccessTokenCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val userId = user.userId
        val claims = listOf(
            Claim(userId, "users", 1),
            Claim(userId, "league", 2),
            Claim(userId, "race", 1)
        )

        val token = jwtService.createAccessToken(user, claims)
        val verified = jwtService.validateAccessToken(token, "users", 2)

        assertSoftly(verified) {
            isLeft()
            it.swap().getOrElse { "" }.shouldBeInstanceOf<InsufficientClaims>()
        }
    }

    "access token is expired as expected"{
        val jwtService = DefaultJWTService(
            Algorithm.HMAC512(
                "mZq4t7w!z%C*F-JaNcRfUjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9yAB&E)H@McQf"
            ),
            Duration.ofDays(1),
            Duration.ofDays(-1),
            "ScalangularAccessTokenCreationTest"
        )

        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            ""
        )

        val userId = user.userId
        val claims = listOf(
            Claim(userId, "users", 1),
            Claim(userId, "league", 2),
            Claim(userId, "race", 1)
        )

        val token = jwtService.createAccessToken(user, claims)
        val verified = jwtService.validateAccessToken(token, "users", 2)

        assertSoftly(verified) {
            isLeft()
            it.swap().getOrElse { "" }.shouldBeInstanceOf<TokenIsExpired>()
        }
    }
})