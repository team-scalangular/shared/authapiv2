package com.scalangular.core.services

import com.scalangular.core.entities.ScalAngularUser
import com.scalangular.core.services.user.BCryptUserService
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.comparables.shouldNotBeEqualComparingTo
import io.kotest.matchers.string.shouldHaveMinLength
import java.util.*

class DefaultUserServiceTest : StringSpec({

    val service = BCryptUserService()

    "password is hashed" {
        val password = "ThisIsThePassword98"
        val hash = service.hashPassword("ThisIsThePassword98")

        assertSoftly(hash) {
            shouldNotBeEqualComparingTo(password)
            shouldHaveMinLength(12)
        }
    }

    "correct password can be verified" {
        val password = "ThisIsTheCorrectPassword"
        val hash = service.hashPassword(password)
        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            hash
        )

        val verified = service.checkPassword(password, user)

        verified.shouldBeTrue()
    }

    "false password can not be verified" {
        val correctPassword = "ThisIsTheCorrectPassword"
        val wrongPassword = "ThisIsAWroGePasSWord"
        val hash = service.hashPassword(correctPassword)
        val user = ScalAngularUser(
            UUID.randomUUID(),
            "TestUser",
            hash
        )

        val verified = service.checkPassword(wrongPassword, user)

        verified.shouldBeFalse()
    }
})